name := "akka--core"

version := "1.0.0"

scalaVersion := "2.11.7"

scalacOptions := Seq("-feature", "-unchecked", "-deprecation", "-encoding", "utf8")

scalacOptions in Test ++= Seq("-Yrangepos")

libraryDependencies ++= Dependencies.depends

enablePlugins(JavaAppPackaging)

Revolver.settings

