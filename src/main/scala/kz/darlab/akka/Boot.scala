package kz.darlab.akka

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import kz.darlab.akka.amqp.{AMQPConnectionCustom, AMQPConsumer, AMQPPublisherImpl, RequestPublisherActor}
import kz.darlab.akka.core.{CoreActor, ElasticsearchDAO}
import kz.darlab.akka.service.ReadCoreAMQPListener

import scala.util.Try


/**
  * Created by nurbek on 9/25/17.
  */

object Boot extends App {

  implicit val system = ActorSystem("akka-core")
  val config = ConfigFactory.load()


  def getStringConfigKey(key: String): String = {
    try {
      sys.env(key)
    } catch {
      case e: NoSuchElementException =>
        config.getString(key)
    }
  }

  def getIntConfigKey(key: String): Int = {
    try {
      sys.env(key).toInt
    } catch {
      case e: NoSuchElementException =>
        config.getInt(key)
    }
  }


  val amqpConnection = new AMQPConnectionCustom(
    getStringConfigKey("amqp.connection.host"),
    getIntConfigKey("amqp.connection.port"),
    getStringConfigKey("amqp.connection.user"),
    getStringConfigKey("amqp.connection.password"),
    getIntConfigKey("amqp.connection.reconnectionDelay"),
    Try {
      getStringConfigKey("amqp.connection.virtualHost")
    }.toOption
  )

  val amqpPublisher = new AMQPPublisherImpl(amqpConnection)
  val replyExchange = config.getString("amqp.endpoints.exchange")
  val publisherActor = system.actorOf(RequestPublisherActor.props(amqpConnection, replyExchange), name = "RequestPublisherActor")

  val elasticsearchDAO = ElasticsearchDAO.apply()

  val readPropsMaker = (_: Unit) =>  CoreActor.props(publisherActor, elasticsearchDAO)

  val readCoreAMQPListener = system.actorOf(ReadCoreAMQPListener.props(amqpPublisher, replyExchange, publisherActor, readPropsMaker), "CoreAMQPListener")


  new AMQPConsumer(amqpConnection, readCoreAMQPListener, "core.read")
  new AMQPConsumer(amqpConnection, readCoreAMQPListener, "core.write")


  system.registerOnTermination {
    system.log.info("DarEco store-repo shutdown.")
  }
}

