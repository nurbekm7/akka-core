package kz.darlab.akka.service

import org.json4s.MappingException
import org.json4s.native.JsonMethods._
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.github.sstone.amqp.Amqp.{Ack, Delivery}
import com.rabbitmq.client.LongString
import kz.darlab.akka.amqp.{AMQPListener, AMQPPerRequestCreator, AMQPPublisherImpl, AMQPRequestContext}
import kz.darlab.akka.domain.core.Request
import kz.darlab.akka.utils.{IdGenerator, SerializersWithTypeHints}


object ReadCoreAMQPListener {

  def props(publisher: AMQPPublisherImpl,
            replyExchange: String,
            publisherActor: ActorRef,
            readPropsMaker: Unit => Props) : Props =
    Props(new ReadCoreAMQPListener(publisher, replyExchange, publisherActor, readPropsMaker))

}

class ReadCoreAMQPListener(publisher: AMQPPublisherImpl,
                        replyExchange: String,
                        publisherActor: ActorRef,
                           readPropsMaker: Unit => Props) extends Actor with ActorLogging with AMQPPerRequestCreator with AMQPListener with SerializersWithTypeHints {

  def receive = {

    case d @ Delivery(consumerTag, envelope, properties, body) => {

      sender() ! Ack(d.envelope.getDeliveryTag)



      val language = if (d.properties.getHeaders.get("Accept-Language") != null) new String(d.properties.getHeaders.get("Accept-Language").asInstanceOf[LongString].getBytes) else "ru"
      val requestContext = AMQPRequestContext(d.properties.getCorrelationId, d.properties.getReplyTo, language)

      val body = new String(d.body)

      log.debug(s"Got response: ${body}")

      try {

        val restMessage = parse(body).extract[Request[_]]

        perRequest(
          requestContext,
          readPropsMaker(),
          restMessage,
          publisher,
          replyExchange,
          publisherActor,
          Some(s"invoice-read-${IdGenerator.getId}")
        )

      } catch {
        case e: MappingException =>
          log.error(s"Failed to parse & extract json. Exception: ${e.toString}")
        case e: Exception =>
          e.printStackTrace()
          log.error(s"Failed to process request. Exception: ${e.toString}")

      }

    }

  }



}
