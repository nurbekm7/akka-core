package kz.darlab.akka.utils

import java.security.SecureRandom
import java.util.UUID

/**
  * Created by Rustem on 26-01-2016.
  */
object IdGenerator {

  private final val key : String = try { sys.env("HOSTNAME") } catch { case e: Exception => UUID.randomUUID().toString }
  val secureRandom = new SecureRandom(key.getBytes)

  def getId : String = {
    val randomBytes: Array[Byte] = new Array[Byte](16)
    secureRandom.nextBytes(randomBytes)
    val uuid = UUID.nameUUIDFromBytes(randomBytes)
    uuid.toString
  }

}