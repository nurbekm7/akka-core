package kz.darlab.akka.utils


import kz.darlab.akka.domain.serializers.{CoreSerializer, DateTimeSerializer}
import org.json4s.{NoTypeHints, ShortTypeHints}
import org.json4s.native.Serialization


trait SerializersWithTypeHints extends DateTimeSerializer  with CoreSerializer  {

  val dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

  implicit val formats = Serialization.formats(ShortTypeHints(coreTypeHints)) +
    new DateTimeSerializer()


}

