package kz.darlab.akka.utils

import kz.darlab.akka.domain.exceptions.{DarErrorSeries, DarErrorSystem}



trait DarEndpoint {
  val errorSystem = DarErrorSystem
  val errorSeries = DarErrorSeries.CORE
}
