package kz.darlab.akka.core

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.sksamuel.elastic4s.Indexable
import com.sksamuel.elastic4s.http.{RequestFailure, RequestSuccess}
import com.sksamuel.elastic4s.http.search.SearchResponse
import kz.darlab.akka.core.CoreActor.Person
import kz.darlab.akka.domain.core.{DomainEntity, Msgs, Request}
import kz.darlab.akka.domain.exceptions.DarErrorCodes.INTERNAL_SERVER_ERROR
import kz.darlab.akka.domain.exceptions.{DarErrorCodes, ServerErrorRequestException}
import kz.darlab.akka.utils.DarEndpoint
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait CoreActorElastic {

  def searchPersonById(id: String): Future[Person]

}

class CoreActorEla(elasticsearchDAO: ElasticsearchDAO) extends CoreActorElastic with DarEndpoint {



  def searchPersonById(id: String): Future[Person] = {
    var person: Person =  null
    val log = LoggerFactory.getLogger(this.getClass)

        elasticsearchDAO.read(id) map {

          case Right(requestSuccess: RequestSuccess[SearchResponse]) =>
            log.info(s"CreatedEvent implemented ${requestSuccess.result.hits.total}")
            val searchResponse = requestSuccess.result
            val searchHit = searchResponse.hits.total
            if (searchHit != 0) {
              searchResponse.hits.hits.foreach { s =>
                val doc = s.sourceAsMap
                person = Person(
                  name = doc("name").asInstanceOf[String]
                )
              }
            }
            person

          case Left(requestFailure: RequestFailure) =>
            log.info(s"CreatedEvent failed of reason: ${requestFailure}")
            throw new ServerErrorRequestException(DarErrorCodes.INTERNAL_SERVER_ERROR(errorSeries, errorSystem))

      }
  }


}


object CoreActor {

  case class Person(name: String)

  def props(publisher: ActorRef, elasticsearchDAO: ElasticsearchDAO): Props = Props(new CoreActor(publisher,elasticsearchDAO))
}

class CoreActor(publisher: ActorRef, elasticsearchDAO: ElasticsearchDAO)  extends  Actor
  with ActorLogging with DarEndpoint  {

  override def receive: Receive =  {

    case Request(replyTo, headers,body,routingKey) =>

      routingKey match {

        case Msgs.Check.routingKey =>

          val coreActorElastic = new CoreActorEla(elasticsearchDAO)
          coreActorElastic.searchPersonById(body.asInstanceOf[Map[String, String]]("id").toString)

      }
  }




}
