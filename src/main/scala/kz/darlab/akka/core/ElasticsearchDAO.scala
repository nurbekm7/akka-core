package kz.darlab.akka.core

import java.text.SimpleDateFormat
import java.util.TimeZone

import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.{ObjectMapper, SerializationFeature}
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.fasterxml.jackson.datatype.joda.cfg.JacksonJodaDateFormat
import com.fasterxml.jackson.datatype.joda.ser.DateTimeSerializer
import com.sksamuel.elastic4s.{ElasticsearchClientUri, Indexable, RefreshPolicy}
import com.sksamuel.elastic4s.http.bulk.BulkResponse
import com.sksamuel.elastic4s.http.search.SearchResponse
import com.sksamuel.elastic4s.http.{HttpClient, RequestFailure, RequestSuccess}
import com.sksamuel.elastic4s.jackson.JacksonSupport
import com.sun.org.apache.bcel.internal.generic.InstructionConstants.Clinit
import com.typesafe.config.ConfigFactory
import kz.darlab.akka.core.CoreActor.Person
import kz.darlab.akka.domain.exceptions.{DarErrorCodes, ServerErrorRequestException}
import kz.darlab.akka.utils.DarEndpoint
import org.joda.time.format.DateTimeFormat
import org.slf4j.LoggerFactory

import scala.concurrent.Future



trait PersonIndexes {

  implicit def JacksonJsonIndexable[T](implicit mapper: ObjectMapper = JacksonSupport.mapper): Indexable[T] = {
    new Indexable[T] {
      mapper.registerModule(new JodaModule().addSerializer(new DateTimeSerializer(new JacksonJodaDateFormat(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")),0)))
      mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
      mapper.setSerializationInclusion(Include.NON_EMPTY)
      mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"))
      mapper.setTimeZone(TimeZone.getTimeZone("Asia/Almaty"))
      override def json(t: T): String = mapper.writeValueAsString(t)
    }
  }

}

object ElasticsearchDAO {


  def apply(): ElasticsearchDAO = {
        val config = ConfigFactory.load()
        val hostname = config.getString("elasticsearch.hostname")
        val port = config.getInt("elasticsearch.port")
        new ElasticsearchDAO(HttpClient(ElasticsearchClientUri(hostname, port)))
      }
}

class ElasticsearchDAO(client: HttpClient) extends  PersonIndexes with DarEndpoint{

    import com.sksamuel.elastic4s.http.ElasticDsl._
    val log = LoggerFactory.getLogger(this.getClass)


  def create(p: Person) = {
    client.execute {
      bulk(
        indexInto("persons" / "person").id("AWU4QXgQFGgIfwxfHyJ_").source(p).refresh(RefreshPolicy.IMMEDIATE)
      )
    }.await match {
      case Right(requestSuccess: RequestSuccess[BulkResponse]) =>
        log.info(s"CreatedEvent implemented $p")
      case Left(requestFailure: RequestFailure) =>
        log.info(s"CreatedEvent failed of $p reason: ${requestFailure}")
        throw  new ServerErrorRequestException(DarErrorCodes.INTERNAL_SERVER_ERROR(errorSeries,errorSystem))

    }
  }

  def read(id: String): Future[Either[RequestFailure, RequestSuccess[_]]] ={
    client.execute {
      search("persons" / "person") query idsQuery(id)
    }
  }

//  def agg(id: String)  ={
//    client.execute {
//      search("invoices" / "paid") query boolQuery.must(
//        termQuery("beneficiary.id.keyword", merchantId),
//        matchQuery("invoiceType.stringVal.keyword", "MAIN"),
//        matchQuery("invoiceStatus.keyword", "DONE"),
//        matchQuery("owner.role.keyword", filter)).filter(
//        rangeQuery("transactionTime").gte(from).lte(to).timeZone("+06:00")) aggs (
//        nestedAggregation("sales_per_category_type", "items") subaggs (
//          termsAgg("categories", "items.catId.keyword")
//            subaggs(
//            termsAgg("items_name", "items.name.keyword")
//              subaggs sumAgg("sales_items", "items.cost").script(ScriptDefinition.string2Script("_value * doc['items.quantity'].value")),
//            sumAgg("sales", "items.cost").script(ScriptDefinition.string2Script("_value * doc['items.quantity'].value")))
//          )
//        )
//    }
//  }





}
