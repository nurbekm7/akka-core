package kz.darlab.akka.amqp

import java.util.Locale

import akka.actor.SupervisorStrategy.{Restart, Stop}
import akka.actor._
import kz.darlab.akka.amqp.AMQPPerRequest.{WithActorRef, WithProps}
import kz.darlab.akka.domain.core._
import spray.http.StatusCode
import spray.http.StatusCodes._
import kz.darlab.akka.domain.exceptions.{ApiException, DarErrorCodes, GatewayTimeoutErrorException, ServerErrorRequestException}
import kz.darlab.akka.exceptions.ApiErrorLocaleContext
import kz.darlab.akka.utils.{DarEndpoint, SerializersWithTypeHints}

import scala.concurrent.duration._


trait AMQPPerRequest extends Actor with ActorLogging with SerializersWithTypeHints with DarEndpoint{

  import context._


  def target: ActorRef
  def message: Request[_]
  def requestContext: AMQPRequestContext
  def publisher: AMQPPublisher
  def replyExchange: String
  def publisherActor: ActorRef

  setReceiveTimeout(60.seconds)
  target ! message

  override def receive = {
    case res: DomainEntity      => complete(OK, res)
    case ReceiveTimeout   => completeWithError(GatewayTimeoutErrorException(DarErrorCodes.INTERNAL_SERVER_ERROR(errorSeries, errorSystem)))
    case ex: ApiException => completeWithError(ex)
  }

  def complete[T <: DomainEntity](status: StatusCode, obj: T) = {
    log.debug(s"Completing request: correlationId=${requestContext.correlationId}, replyTo=${requestContext.replyTo}")
//    val data: String = write[T](obj)
//    publisher.publish(data, requestContext.replyTo, replyExchange, Some(requestContext.correlationId))
    val dm = DomainMessage.response[T](message, obj)
    publisherActor ! RequestPublisherActor.PublishToQueue(dm.get, Some(requestContext.correlationId))
    stop(self)
  }

  /**
    * Completes request with error
    *
    * @param apiException
    */
  def completeWithError(apiException: ApiException) = {
    val language = requestContext.language
    val errorInfo = apiException.getErrorInfo(Some(new ApiErrorLocaleContext(new Locale(language))))
    log.error(s"Application return expected error status code [${language}] ${apiException.status} with entity ${errorInfo} ")
//    complete(apiException.status, errorInfo)
    val dm = DomainMessage.error(errorInfo, Some(requestContext.replyTo), message.headers)
    publisherActor ! RequestPublisherActor.PublishToQueue(dm, Some(requestContext.correlationId))
    stop(self)
  }

  override val supervisorStrategy =

    OneForOneStrategy() {

      /**
        * Catching Api Exceptions
        */
      case e: ApiException => {

        completeWithError(e)

        Stop
      }

      case e: NullPointerException => {
        log.error(s"${e.getStackTrace}")
        Restart
      }

      /**
        * Catching any other exceptions
        */
      case e => {

        log.error(s"${e.getStackTrace}")

        val error = ServerErrorRequestException(DarErrorCodes.INTERNAL_SERVER_ERROR(errorSeries, errorSystem), Some(e.getMessage))
        completeWithError(error)

        Stop

      }

    }

}

object AMQPPerRequest {

  case class WithActorRef(requestContext: AMQPRequestContext, target: ActorRef, message: Request[_], publisher: AMQPPublisher, replyExchange: String, publisherActor: ActorRef) extends AMQPPerRequest

  case class WithProps(requestContext: AMQPRequestContext, props: Props, message: Request[_], publisher: AMQPPublisher, replyExchange: String, publisherActor: ActorRef, actorName: Option[String] = None) extends AMQPPerRequest {
    lazy val target = actorName match {
      case Some(name) => context.actorOf(props, name)
      case _ => context.actorOf(props)
    }
  }

}

trait AMQPPerRequestCreator {
  this: Actor =>

  def perRequest(r: AMQPRequestContext,
                 target: ActorRef,
                 message: Request[_],
                 publisher: AMQPPublisher,
                 replyExchange: String,
                 publisherActor: ActorRef) =
    context.actorOf(Props(new WithActorRef(r, target, message, publisher: AMQPPublisher, replyExchange, publisherActor)))

  def perRequest(r: AMQPRequestContext,
                 props: Props,
                 message: Request[_],
                 publisher: AMQPPublisher,
                 replyExchange: String,
                 publisherActor: ActorRef,
                 actorName: Option[String] = None) =
  actorName match {
    case Some(name) => context.actorOf(Props(new WithProps(r, props, message, publisher: AMQPPublisher, replyExchange, publisherActor, actorName)), s"parent-$name")
    case _ => context.actorOf(Props(new WithProps(r, props, message, publisher: AMQPPublisher, replyExchange, publisherActor)))
  }

}