package kz.darlab.akka.amqp


case class AMQPRequestContext(correlationId: String, replyTo: String, language: String)