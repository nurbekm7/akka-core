package kz.darlab.akka.amqp


import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, Props}
import akka.util.Timeout
import com.github.sstone.amqp.Amqp.Publish
import com.github.sstone.amqp.{Amqp, ChannelOwner, ConnectionOwner}
import com.rabbitmq.client.AMQP
import kz.darlab.akka.amqp.RequestPublisherActor.PublishToQueue
import kz.darlab.akka.domain.core._
import kz.darlab.akka.utils.SerializersWithTypeHints
import org.json4s.native.Serialization._

import scala.collection.JavaConversions._
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

import akka.pattern.ask

object RequestPublisherActor {

  case class PublishToQueue(dm: DomainMessage[_], correlationId: Option[String] = None)

  case class Success(correlationId: Option[String] = None)

  case class Failure(reason: Throwable, correlationId: Option[String] = None)

  def props(amqpConnection: AMQPConnection, outboundGate: String): Props = Props(classOf[RequestPublisherActor], amqpConnection, outboundGate)

}

class RequestPublisherActor(amqpConnection: AMQPConnection, outboundGate: String) extends Actor with SerializersWithTypeHints with ActorLogging {
  import context._

  val producer = ConnectionOwner.createChildActor(amqpConnection.conn, ChannelOwner.props())

  Amqp.waitForConnection(system, amqpConnection.conn, producer).await(5, TimeUnit.SECONDS)

  override def receive: Receive = {

    case PublishToQueue(dm, correlationId)  =>

      val senderRef = sender()

      dm match {
        case req : Request[_] =>

          val data = write[DomainMessage[_]](dm)

          log.info(data)

          publish(
            data = data,
            routingKey = req.routingKey,
            exchange = outboundGate,
            correlationId = correlationId,
            replyTo = Some(req.replyTo.get),
            headers = Some(dm.headers.map(m => (m._1, m._2.asInstanceOf[AnyRef])))
          ) onComplete {
            case Success(Right(ok)) => senderRef ! RequestPublisherActor.Success(correlationId)
            case Success(Left(err)) => senderRef ! RequestPublisherActor.Failure(err.reason, correlationId)
            case Failure(ex) => senderRef ! RequestPublisherActor.Failure(ex, correlationId)
          }

        case res: Response[_] =>
          val data = write[DomainMessage[_]](dm)

          log.info(data)

          publish(
            data = data,
            routingKey = res.routingKey,
            exchange = outboundGate,
            correlationId = correlationId,
            headers = Some(dm.headers.map(m => (m._1, m._2.asInstanceOf[AnyRef])))
          ) onComplete {
            case Success(Right(ok)) => senderRef ! RequestPublisherActor.Success(correlationId)
            case Success(Left(err)) => senderRef ! RequestPublisherActor.Failure(err.reason, correlationId)
            case Failure(ex) => senderRef ! RequestPublisherActor.Failure(ex, correlationId)
          }
          //TODO make sending response


        case err: Error =>

          val data = write[DomainMessage[_]](dm)

          log.debug(data)

          publish(
            data = data,
            routingKey = err.routingKey,
            exchange = outboundGate,
            correlationId = correlationId,
            headers = Some(dm.headers.map(m => (m._1, m._2.asInstanceOf[AnyRef])))
          ) onComplete {
            case Success(Right(ok)) => senderRef ! RequestPublisherActor.Success(correlationId)
            case Success(Left(err)) => senderRef ! RequestPublisherActor.Failure(err.reason, correlationId)
            case Failure(ex) => senderRef ! RequestPublisherActor.Failure(ex, correlationId)
          }



      }

  }

  def publish(data            : String,
              routingKey      : String,
              exchange        : String,
              correlationId   : Option[String] = None,
              replyTo         : Option[String] = None,
              messageId       : Option[String] = None,
              `type`          : Option[String] = None,
              contentType     : String = "application/json",
              contentEncoding : String = "UTF-8",
              headers         : Option[Map[String, AnyRef]] = None) : Future[Either[Amqp.Error, Amqp.Ok]] = {

    implicit val timeout: Timeout = 60 seconds

    val body = data.getBytes

    val javaHeaders: java.util.Map[String, AnyRef] = headers match {
      case Some(h) => mapAsJavaMap[String, AnyRef](h)
      case _ => null
    }

    val properties = new AMQP.BasicProperties.Builder()
      .contentType(contentType)
      .contentEncoding(contentEncoding)
      .messageId(messageId.orNull)
      .replyTo(replyTo.orNull)
      .correlationId(correlationId.orNull)
      .`type`(`type`.orNull)
      .headers(javaHeaders)
      .build()

    val res = producer ? Publish(exchange, routingKey, body, Some(properties))

    res.map {
      case m@Amqp.Ok(request, result) => Right(m)
      case m@Amqp.Error(request, reason) => Left(m)
    }

  }

}
