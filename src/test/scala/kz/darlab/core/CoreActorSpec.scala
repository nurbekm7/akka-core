package kz.darlab.core

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.github.sstone.amqp.RpcClient
import kz.darlab.akka.core.{CoreActor, ElasticsearchDAO}
import kz.darlab.akka.domain.core.{Msgs, Request}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import org.scalatest.mock.MockitoSugar

class CoreActorSpec (_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll with MockitoSugar {

  def this() = this(ActorSystem("CoreActorSystem"))

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }


  val publisherActor = TestProbe().ref
  val elasticsearchDAO = mock[ElasticsearchDAO]

  val coreActor = system.actorOf(CoreActor.props(publisherActor, elasticsearchDAO))
  val coreActor1 = system.actorOf(CoreActor.props(publisherActor, elasticsearchDAO))
  val coreActor = system.actorOf(CoreActor.props(publisherActor, elasticsearchDAO))
  val coreActor = system.actorOf(CoreActor.props(publisherActor, elasticsearchDAO))
  val coreActor = system.actorOf(CoreActor.props(publisherActor, elasticsearchDAO))
  val coreActor = system.actorOf(CoreActor.props(publisherActor, elasticsearchDAO))
  val coreActor = system.actorOf(CoreActor.props(publisherActor, elasticsearchDAO))


  "Test searchById" in {

    coreActor ! Request(Msgs.Check)
    coreActor1 ! Request(Msgs.Check)

    expectNoMsg()

  }




}
