package kz.darlab.core

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestKit, TestProbe}
import com.sksamuel.elastic4s.http.{RequestSuccess, Shards}
import com.sksamuel.elastic4s.http.search.{SearchHit, SearchHits, SearchResponse}
import kz.darlab.akka.amqp.RequestPublisherActor
import kz.darlab.akka.core.CoreActor.Person
import kz.darlab.akka.core.{CoreActor, CoreActorEla, CoreActorElastic, ElasticsearchDAO}
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class ExampleSpec extends Specification with Mockito  {
  //  override def is = s2"""
  //
  // This is a specification to check the 'Hello world' string
  //
  // The 'Hello world' string should
  //   contain 11 characters                                         $e1
  //   start with 'Hello'                                            $e2
  //   end with 'world'                                              $e3
  //                                                                 """
  //
  //  def e1 = "Hello world" must have size(11)
  //  def e2 = "Hello world" must startWith("Hello")
  //  def e3 = "Hello world" must endWith("world")

  val elasticDAO = mock[ElasticsearchDAO].verbose
  val clientCore = new CoreActorEla(elasticDAO)

  elasticDAO.read(anyString) returns Future{Right(RequestSuccess(200, None, Map(), SearchResponse(5,false,false,null,
        Shards(5,0,5),None,null, SearchHits(total = 2,maxScore = 2.0, hits = Array(SearchHit("AWU4RFcTFGgIfwxfHyKB","persons","person",
      1.0f, None,None,Map("name" -> "S"),null,null,null,1)))) )) }

  "CoreActorTrait initialization" >> {

    "with correct namespace and set name" >> {

      var result = Person("")
     clientCore.searchPersonById("1") map {res =>  result = res}
      result must be equalTo Person("S")
    }

  }

}

//import collection.mutable.Stack
//import org.scalatest._
//
//class ExampleSpec extends FlatSpec with Matchers {
//
//  "A Stack" should "pop values in last-in-first-out order" in {
//    val stack = new Stack[Int]
//    stack.push(1)
//    stack.push(2)
//    stack.pop() should be (2)
//    stack.pop() should be (1)
//  }
//
//  it should "throw NoSuchElementException if an empty stack is popped" in {
//    val emptyStack = new Stack[Int]
//    a [NoSuchElementException] should be thrownBy {
//      emptyStack.pop()
//    }
//  }
//
//}