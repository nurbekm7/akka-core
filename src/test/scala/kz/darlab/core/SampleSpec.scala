package kz.darlab.core


import org.specs2.mock.Mockito
import org.specs2.mutable.Specification


trait SampleClass {
  def add(a: Int, b: Int): Int = a + b
}
object SampleClass extends SampleClass


class SampleSpec extends Specification with Mockito {
  "Mock" should {
    "mock" in {
      val m = mock[SampleClass]
      m.add(anyInt, anyInt) returns 1000

      m.add(1, 1) must be equalTo 1000
    }
  }
}
